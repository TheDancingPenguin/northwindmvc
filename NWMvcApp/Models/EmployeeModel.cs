﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NWMvcApp.Models
{
    public class EmployeeModel
    {
        public int EmployeeId { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string Title { get; set; }
        [Required]
        public DateTime HireDate { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Postal { get; set; }
        public List<Employee> EmployeeList { get; set; }
    }
}