﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NWMvcApp.Models
{
    public class ProductModel
    {
        public List<Product> ProductList { get; set; }
        public SelectList SupplierSelectList { get; set; }
        public int SelectedSupplierItem { get; set; }

        public SelectList CategorySelectList { get; set; }
        public int SelectedCategoryItem { get; set; }

        public int ProductID { get; set; }
        public int? SupplierID { get; set; }

        [Required]
        public string ProductName { get; set; }
        public Supplier Supplier { get; set; }
        
        [Required]
        public int CategoryID { get; set; }
        
        public Category Category { get; set; }
        public string QuantityPerUnit { get; set; }
        public decimal? UnitPrice { get; set; }
        public Int16? UnitsInStock { get; set; }
        public Int16? UnitsOnOrder { get; set; }
        public Int16? ReorderLevel { get; set; }
        
        [Required]
        public bool Discontinued { get; set; }   
    }
}