﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NWMvcApp.Models;

namespace NWMvcApp.Controllers
{
    public class ProductController : Controller
    {
        //
        // GET: /Product/

        public ActionResult Index()
        {

            var entities = new NWEntities();
            var products = entities.Products;

            return View(new ProductModel
            {
                ProductList = products.ToList()
            });
        }

        //
        // GET: /Product/Details/5

        public ActionResult Details(int id)
        {
            var entities = new NWEntities();
            var product = entities.Products
                .Include("Supplier")
                .Include("Category")
                .SingleOrDefault(x => x.ProductID == id);
            
            
            return View(new ProductModel
            {
                ProductName = product.ProductName,
                SupplierID = product.SupplierID,
                Supplier = product.Supplier,
                Category = product.Category,
                QuantityPerUnit = product.QuantityPerUnit,
                UnitPrice = product.UnitPrice,
                UnitsInStock = product.UnitsInStock,
                UnitsOnOrder = product.UnitsOnOrder,
                ReorderLevel = product.ReorderLevel,
                Discontinued = product.Discontinued
            });
        }

        //
        // GET: /Product/Create

        [HttpGet]
        public ActionResult Create()
        {
            if (Session["SuppliersList"] == null || Session["CategoriesList"]==null)
            {
                GetSuppliersAndCats();
            }

            return View(new ProductModel
            {
                SupplierSelectList = GetSupplierSelectList((List<Supplier>)Session["SuppliersList"]),
                CategorySelectList = GetCategorySelectList((List<Category>)Session["CategoriesList"])
            });
        }

        //
        // POST: /Product/Create
        [HttpPost]
        public ActionResult Create(ProductModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(new ProductModel
                    {
                        SupplierSelectList = GetSupplierSelectList((List<Supplier>)Session["SuppliersList"]),
                        CategorySelectList = GetCategorySelectList((List<Category>)Session["CategoriesList"])
                    });
                }


                
                var entities = new NWEntities();
                entities.Products.Add(new Product
                {
                    ProductName = model.ProductName,
                    SupplierID = model.SelectedSupplierItem,
                    CategoryID = model.SelectedCategoryItem,
                    QuantityPerUnit = model.QuantityPerUnit,
                    UnitPrice = model.UnitPrice,
                    UnitsInStock = model.UnitsInStock,
                    UnitsOnOrder = model.UnitsOnOrder,
                    ReorderLevel = model.ReorderLevel,
                    Discontinued = model.Discontinued
                });
                entities.SaveChanges();


                return View(new ProductModel
                {
                    SupplierSelectList = GetSupplierSelectList((List<Supplier>)Session["SuppliersList"]),
                    CategorySelectList = GetCategorySelectList((List<Category>)Session["CategoriesList"])
                });
            }
            catch
            {
                return View(new ProductModel
                {
                    SupplierSelectList = GetSupplierSelectList((List<Supplier>)Session["SuppliersList"]),
                    CategorySelectList = GetCategorySelectList((List<Category>)Session["CategoriesList"])
                });
            }
        }

        //
        // GET: /Product/Edit/5

        public ActionResult Edit(int id)
        {
            //return View();
            return null;
        }

        //
        // POST: /Product/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                //return View();
                return null;
            }
        }

        //
        // GET: /Product/Delete/5

        public ActionResult Delete(int id)
        {
            //return View();
            return null;
        }

        //
        // POST: /Product/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                //return View();
                return null;
            }
        }

        private static SelectList GetSupplierSelectList(IEnumerable<Supplier> suppliers)
        {
            var items = suppliers.Select(o => new { Value = o.SupplierID, Text = o.ContactName });
            return new SelectList(items, "Value", "Text");
        }

        private static SelectList GetCategorySelectList(IEnumerable<Category> categories)
        {
            var items = categories.Select(o => new { Value = o.CategoryID, Text = o.CategoryName });
            return new SelectList(items, "Value", "Text");
        }

        private void GetSuppliersAndCats()
        {
            var entities = new NWEntities();
            var suppliers = entities.Suppliers.ToList<Supplier>();
            var categories = entities.Categories.ToList<Category>();

            Session["SuppliersList"] = suppliers;
            Session["CategoriesList"] = categories;        
        }

    }

}
