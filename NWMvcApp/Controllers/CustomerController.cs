﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NWMvcApp.Models;

namespace NWMvcApp.Controllers
{
    public class CustomerController : Controller
    {
        //
        // GET: /Customer/

        public ActionResult Index()
        {
            var entities = new NWEntities();
            var customers = entities.Customers;

            return View(
                new CustomerModel
                {
                    CustomerList = customers.ToList()
                });
        }

        //
        // GET: /Customer/Details/5

        public ActionResult Details(int id)
        {
            //return View();
            return null;
        }

        //
        // GET: /Customer/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Customer/Create

        [HttpPost]
        public ActionResult Create(CustomerModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return View();
                }

                var entities = new NWEntities();
                entities.Customers.Add(new Customer
                {
                    CustomerID = model.CustomerId,
                    CompanyName = model.CompanyName,
                    ContactName = model.ContactName
                });

                entities.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }

        //
        // GET: /Customer/Edit/5

        public ActionResult Edit(int id)
        {
            //return View();
            return null;
        }

        //
        // POST: /Customer/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                //return View();
                return null;
            }
        }

        //
        // GET: /Customer/Delete/5

        public ActionResult Delete(int id)
        {
            //return View();
            return null;
        }

        //
        // POST: /Customer/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                //return View();
                return null;
            }
        }
    }
}
