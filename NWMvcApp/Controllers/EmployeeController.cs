﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NWMvcApp.Models;

namespace NWMvcApp.Controllers
{
    public class EmployeeController : Controller
    {
        //
        // GET: /Employee/

        public ActionResult Index()
        {
            var entities = new NWEntities();
            var emps = entities.Employees;
            
            return View(new EmployeeModel
            {
                EmployeeList = emps.ToList()
            });
        }

        //
        // GET: /Employee/Details/5

        public ActionResult Details(int id)
        {
            //return View();
            return null;
        }

        //
        // GET: /Employee/Create

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Employee/Create

        [HttpPost]
        public ActionResult Create(EmployeeModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View();
                }

                var entities = new NWEntities();
                entities.Employees.Add(
                    new Employee
                    {
                        LastName = model.LastName,
                        FirstName = model.FirstName,
                        Title = model.Title,
                        HireDate = model.HireDate,
                        BirthDate = model.BirthDate,
                        Address = model.Address,
                        City = model.City,
                        Region = model.Region,
                        PostalCode = model.Postal
                    });

                entities.SaveChanges();


                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Employee/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Employee/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                //return View();
                return null;
            }
        }

        //
        // GET: /Employee/Delete/5

        public ActionResult Delete(int id)
        {
            //return View();
            return null;
        }

        //
        // POST: /Employee/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                //return View();
                return null;
            }
        }
    }
}
